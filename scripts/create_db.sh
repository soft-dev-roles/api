#!/usr/bin/env sh


if [[ ! -f $1 ]]; then
  echo "Environment file '$1' does not exist"
  exit 1
fi
if [[ ! -s $1 ]]; then
  echo "Environment file '$1' is empty"
  exit 1
fi
if [[ ! -d $2 ]]; then
  echo "Directory '$2' does not exit"
  exit 1
fi

envfile=$(realpath $1)
sqlfiles=$(realpath $2)

# check link https://medium.com/@joegoosebass/how-to-read-environment-variables-with-bash-45eccfb31a58
while read var; do
  if [[ ! ${var:0:1} = "#" ]]; then
    k=${var%%=*}
    v=${var#*=}
    if [[ ! -z "$k" ]]; then
      # echo "$k=$v"
      export $k=$v
    fi
  fi
done < $envfile

sudo mysql -u root -e "CREATE DATABASE ${DB_NAME};";
if [[ $? -ne 0 ]]; then
  exit 1
fi
sudo mysql -u root -e "GRANT ALL PRIVILEGES ON ${DB_NAME}.* TO \"${DB_USER}\"@\"${DB_HOST}\" IDENTIFIED BY \"${DB_PASS}\";"

# cat $sqlfiles/*.sql | mysql -u "${DB_USER}" --password="${DB_PASS}" "${DB_NAME}"
mysql -u "${DB_USER}" --password="${DB_PASS}" "${DB_NAME}" < ${sqlfiles}/PYKA_title.sql
mysql -u "${DB_USER}" --password="${DB_PASS}" "${DB_NAME}" < ${sqlfiles}/PYKA_unit.sql
mysql -u "${DB_USER}" --password="${DB_PASS}" "${DB_NAME}" < ${sqlfiles}/PYKA_staff.sql
mysql -u "${DB_USER}" --password="${DB_PASS}" "${DB_NAME}" < ${sqlfiles}/PYKA_roles.sql
mysql -u "${DB_USER}" --password="${DB_PASS}" "${DB_NAME}" < ${sqlfiles}/roles_expired.sql
