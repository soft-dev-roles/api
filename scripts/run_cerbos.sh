#!/usr/bin/env sh

docker run --name cerbos -d \
  -v $(pwd)/cerbos:/api \
  -p 3592:3592 ghcr.io/cerbos/cerbos:0.11.0 \
  server --config=/api/conf.yaml
