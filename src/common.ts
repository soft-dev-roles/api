import { Principal } from "./cerbos";
import prisma from "./data";

export class Context {
  principal = new Principal();
}

export async function getEditorInUnits(principal: Principal) {
  const editorInUnits = new Set<string>();
  const units         = new Set<string>(
    await prisma.roles.findMany({
      select: { unitId: true },
      where:  {
        apmId:   principal.id,
        title: { unitEditor: true }
      }
    }).then(result => result.map(unit => unit.unitId))
  );

  while (units.size > 0) {
    const newUnits = await prisma.unit.findMany({
      select: { id: true },
      where:  { parentUnitId: {
        in: Array.from(units.values()).map(unitId => parseInt(unitId))
      }}
    }).then(result => result.map(newUnit => newUnit.id));

    units.forEach(unitId => editorInUnits.add(unitId));
    units.clear();
    newUnits.forEach(unitId => {
      if (!editorInUnits.has(unitId)) {
        editorInUnits.add(unitId);
        units.add(unitId);
      }
    });
  }

  return Array.from(editorInUnits.values());
}
