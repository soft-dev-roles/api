import { staff, roles } from '@prisma/client';
import { Cerbos } from 'cerbos';


const cerbos = new Cerbos({
  hostname: "http://localhost:3592"
});

export class Principal {
  constructor(
    public id:    string   = "",
    public roles: string[] = [],
    public attr?: { [key: string]: any }
  )
  { }

  staffToPrincipal(user: staff) {
    this.id    = user.apmId;
    this.roles.push("user");
    this.attr  = { ...user };
  }
};

export class Resource<T> {
  public instances: { [key: string]: { attr?: T } } = {};

  constructor(
    public kind    = "",
    instances: T[] = [],
    instanceKeyName: string
  ) {
    for (const instance of instances) {
      type instanceKey = keyof typeof instance;
      let idValue = instance[instanceKeyName as instanceKey];
      let id = "";

      switch (typeof idValue) {
        case "symbol":
          id = idValue.toString();  
          break;
        case "number":
          id = idValue.toString();  
          break;
        case "string":
          id = idValue;
        default:
          break;
      }
      this.instances[id] = { attr: instance };
    }
  }
};

export async function check<T>(principal: Principal, resources: Resource<T>, actions: string[], id: string) {
  return cerbos.check({
    principal: principal,
    resource: resources,
    actions: actions
  }).then(handle => actions.map(action => handle.isAuthorized(id, action)));
}
