import { ApolloServer } from 'apollo-server';
import { config } from 'dotenv';
import { Context } from './common';
import prisma from './data';
import schema from './graphql/schema';


config();

const server = new ApolloServer({
  schema: schema,
  cors: {
    origin: '*'
  },
  context: async ({ req }) => {
    let context = new Context();

    if (req.headers.authorization) {
      await prisma.staff.findUnique({
        where: { apmId: req.headers.authorization }
      }).then(user => {
        if (user) context.principal.staffToPrincipal(user);
      });
    }
    return context;    
  },
  formatError: (error) => {
    return { message: error.message, locations: error.locations };
  }
});

server.listen({ port: process.env.API_PORT })
  .then(({ url }) => {
    console.log(`Server is listening on ${url}`);
  });