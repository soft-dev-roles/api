import { DateTimeScalarConfig, Resolvers, Roles } from "./generated/generated-backend";
import prisma from "../data";
import { GraphQLScalarType } from "graphql";
import { Context, getEditorInUnits } from "../common";
import { check, Resource } from "../cerbos";


const config: DateTimeScalarConfig = {
  name: "DateTime",

  /**
   * Serialize js `Date` object, converting it to string.
   * @param value graphql field
   * @returns `string` ISO representation
   * @throws when the value is not an instance of a `Date`
   */
  serialize: (value) => {
    if (value instanceof Date)
      return value.toISOString();
    throw new Error("It's not a date");
  },

  /**
   * Parse a value based on its Datatype.
   * 
   * String values trigger the `isNan()` predicate.
   * If `value` is a number, it is expected to be a UNIX timestamp in seconds.
   * It is converted to milliseconds for the construction of a `Date` object
   * @param value 
   * @returns 
   */
  parseValue: (value) => {
    if (isNaN(value)) return new Date(value);
    return new Date(<number>value * 1000);
  },

  /**
   * Parse the value given by the variables object in request's body
   * @param value date to be parsed
   * @returns `Date` value
   */
  parseLiteral: (value) => {
    switch (value.kind) {
      case "StringValue":
        return new Date(value.value);
      case "IntValue":
        return new Date(parseInt(value.value) * 1000);
      default:
        return null;
    }
  }
}

const resolvers: Resolvers<Context> = {
  DateTime: new GraphQLScalarType({
    ...config,
  }),

  Title: {
    /**
     * Find all the `Roles` with a `role` given by the query's parent `id`
     * @param parent `Title`
     * @returns 'Roles' assosiated with the parent `Title`
     */
    roles: (parent) => {
      return prisma.roles.findMany({ where: { role: parent.id } });
    }
  },

  Unit: {
    /**
     * Find all the `Roles` with a `unitId` given by the query's parent `id`
     * @param parent `Unit`
     * @returns `Roles` assosiated with the parent `Unit`
     */
    roles: (parent) => {
      return prisma.roles.findMany({ where: { unitId: parent.id } });
    }
  },

  Staff: {
    /**
     * Find all the `Roles` with a `apmId` given by the query's parent `apmId`
     * @param parent `Staff`
     * @returns `Roles` assosiated with the parent `Staff`
     */
    roles: (parent) => {
      return prisma.roles.findMany({ where: { apmId: parent.apmId } });
    },

    /**
     * Find all the `Unit`s with a `id` given by the query's parent `unitId`
     * @param parent `Staff`
     * @returns `Unit`s assosiated with the parent `Staff`
     */
    unit: (parent) => {
      return prisma.unit.findUnique({ where: { id: parent.unitId } });
    },

    /**
     * Find all the `RolesExpired` with a `staff` relation to the parent
     * @param parent `Staff`
     * @returns `RolesExpired` assosiated with the parent `Staff`
     */
    roles_expired: (parent) => {
      return prisma.roles_expired.findMany({ where: { staff: parent } });
    }
  },

  Roles: {
    /**
     * Find the `Title` using the table relation `Roles - Title`
     * @param parent `Roles`
     * @returns `Title` assosiated with this `Roles` row
     */
    title: (parent) => {
      return prisma.title.findUnique({ where: { id: parent.role } })
        .then(result => result!);
    },

    /**
     * Find the `Unit` using the table relation `Roles - Unit`
     * @param parent `Roles`
     * @returns `Unit` assosiated with this `Roles` row
     */
    unit: (parent) => {
      return prisma.unit.findUnique({ where: { id: parent.unitId } })
        .then(result => result!);
    },

    /**
     * Find the `Staff` using the table relation `Roles - Staff`
     * @param parent `Roles`
     * @returns `Staff` assosiated with this `Roles` row
     */
    staff: (parent) => {
      return prisma.staff.findUnique({ where: { apmId: parent.apmId } });
    }
  },

  RolesExpired: {
    /**
     * Find the `Title` using the table relation `RolesExpired - Title`
     * @param parent `Roles`
     * @returns `Title` assosiated with this `RolesExpired` row
     */
    title: (parent) => {
      return prisma.title.findUnique({ where: { id: parent.role } })
        .then(result => result!);
    },

    /**
     * Find the `Unit` using the table relation `RolesExpired - Unit`
     * @param parent `Roles`
     * @returns `Unit` assosiated with this `RolesExpired` row
     */
    unit: (parent) => {
      return prisma.unit.findUnique({ where: { id: parent.unitId } })
        .then(result => result!);
    },

    /**
     * Find the `Staff` using the table relation `RolesExpired - Staff`
     * @param parent `Roles`
     * @returns `Staff` assosiated with this `RolesExpired` row
     */
    staff: (parent) => {
      return prisma.staff.findUnique({ where: { apmId: parent.apmId } });
    }
  },

  Query: {
    titles: () => prisma.title.findMany({ orderBy: {nameEn: 'asc'} }),

    units:  () => prisma.unit.findMany(),

    /**
     * Retrive all the 'Staff' from the database.
     * If `args.apmId` is set, get an array of one `Staff` object
     * with the given `apmId`
     * @param args optional `apmId` of `Staff`
     * @returns `Staff[]` objects
     */
    staff: async (_, args) => {
      if (args.apmId) {
        const staff = await prisma.staff.findUnique({
          where: { apmId: args.apmId }
        });
        if (staff) return [staff];
        return [];
      }
      return prisma.staff.findMany();
    },

    roles: () => prisma.roles.findMany(),

    staff2: (_, args) => {
      let name     = args.name    ? args.name!    : undefined;
      let unitId   = args.unitId  ? args.unitId!  : undefined;
      let titlesId = args.titleId ? args.titleId! : undefined;

      if (titlesId?.length == 0 || (titlesId?.length == 1 && titlesId[0] === ""))
        titlesId = undefined;
      if (name === "")
        name    = undefined;
        
      if (!(unitId || titlesId || name))
        return prisma.staff.findMany();
      if (name) {
        if (!(unitId || titlesId))
          return prisma.staff.findMany({
            where: {
              OR: [
                {firstEn: { contains: name }},
                {lastEn:  { contains: name }},
                {first:   { contains: name }},
                {last:    { contains: name }}
              ]
            }
          });
        return prisma.staff.findMany({
          where: {
            roles: {
              some: { unitId: unitId, role: { in: titlesId } },
            },
            OR: [
              {firstEn: { contains: name }},
              {lastEn:  { contains: name }},
              {first:   { contains: name }},
              {last:    { contains: name }},
            ]
          }
        });
      }
      return prisma.staff.findMany({
        where: {
          roles: {
            some: { unitId: unitId, role: { in: titlesId } },
          },
        }
      });
    }
  },
  Mutation: {
    createTitle: {
      resolve: (parent, args, context, info) => {
        return new Date(args.lastUpdated);
      }
    },
    expireRole: async (_, args, context) => {
      const role = await prisma.roles.findUnique({ where: { id: args.id } });
      if (role) {
        if (! await prisma.roles.delete({ where: { id: role.id } }))
          return null;
        return prisma.roles_expired.create({
          data: {
            apmId:  role.apmId,
            role:   role.role,
            unitId: role.unitId,
            start:  role.start,
            end:    role.end,
            updatedBy: context.principal.id,
          }
        });
      }
      return null;
    },
    restoreRole: async (_, args, context) => {
      const expired = await prisma.roles_expired.findUnique({where: {id: args.id}});
      if (expired) {
        if (! await prisma.roles_expired.delete({where: {id: expired.id}}))
          return null;
        return prisma.roles.create({
          data: {
            apmId:  expired.apmId,
            role:   expired.role,
            unitId: expired.unitId,
            start:  expired.start,
            updatedBy: context.principal.id,
          }
        })
      }
      return null;
    },


    addRole: async (_, args, context) => {
      if (context.principal.attr) {
        context.principal.attr.editorInUnits = await getEditorInUnits(context.principal);
      }
      const newRole  = { apmId: args.apmId, unitId: args.unitId, role: args.role };
      const resource = new Resource("roles", [newRole], "apmId");
      const actions  = ["create"];
      const [isAllowed] = await check(context.principal, resource, actions, args.apmId);
      if (!isAllowed) return null;

      let titleProm = prisma.title.findUnique({
        select: { id: true },
        where:  { id: args.role }
      });
      let unitProm  = prisma.unit.findUnique({
        select: { id: true },
        where:  { id: args.unitId }
      });
      let staffProm = prisma.staff.findUnique({
        select: { apmId: true },
        where:  { apmId: args.apmId }
      });
      const [title, unit, staff] = await Promise.all([titleProm, unitProm, staffProm]);

      if (!(title && unit)) return null;

      return prisma.roles.create({
        data: {
          title: { connect: { id: title.id } },
          unit:  { connect: { id: unit.id  } },
          staff: { connect: { apmId: staff?.apmId } },
          updatedBy: context.principal.id,
          start: new Date()
        }
      }).catch(error => {
        const match = String(error).match(/Unique.*$/);
        if (match) throw new Error(match.at(0));
        throw error;
      });
    }
  }
};

export default resolvers;