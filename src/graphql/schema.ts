import { join } from 'path';
import { loadSchemaSync } from '@graphql-tools/load';
import { GraphQLFileLoader } from '@graphql-tools/graphql-file-loader';
import { addResolversToSchema } from '@graphql-tools/schema';
import resolvers from './resolvers';


let schema = loadSchemaSync(join(__dirname, 'schema/schema.gql'), {
  loaders: [new GraphQLFileLoader()]
});

schema = addResolversToSchema({schema: schema, resolvers: resolvers});

export default schema;
