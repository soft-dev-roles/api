import { title_type } from 'node_modules/.prisma/client/index';
import { title_relUnitType } from 'node_modules/.prisma/client/index';
import { title_titleCategory } from 'node_modules/.prisma/client/index';
import { title_affiliation } from 'node_modules/.prisma/client/index';
import { unit_isAcademic } from 'node_modules/.prisma/client/index';
import { unit_type } from 'node_modules/.prisma/client/index';
import { unit_isStem } from 'node_modules/.prisma/client/index';
import { unit_syncAdmin } from 'node_modules/.prisma/client/index';
import { unit_unitStatus } from 'node_modules/.prisma/client/index';
import { unit_noAccountCreate } from 'node_modules/.prisma/client/index';
import { unit_primDns } from 'node_modules/.prisma/client/index';
import { staff_status } from 'node_modules/.prisma/client/index';
import { staff_gender } from 'node_modules/.prisma/client/index';
import { GraphQLResolveInfo, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';
import { title, unit, staff, roles, roles_expired } from 'node_modules/.prisma/client/index';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type RequireFields<T, K extends keyof T> = { [X in Exclude<keyof T, K>]?: T[X] } & { [P in K]-?: NonNullable<T[P]> };
export type EnumResolverSignature<T, AllowedValues = any> = { [key in keyof T]?: AllowedValues };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: Date;
};

export type Mutation = {
  __typename?: 'Mutation';
  addRole?: Maybe<Roles>;
  createTitle?: Maybe<Scalars['DateTime']>;
  expireRole?: Maybe<RolesExpired>;
  restoreRole?: Maybe<Roles>;
};


export type MutationAddRoleArgs = {
  apmId: Scalars['String'];
  role: Scalars['ID'];
  unitId: Scalars['ID'];
};


export type MutationCreateTitleArgs = {
  lastUpdated: Scalars['DateTime'];
};


export type MutationExpireRoleArgs = {
  id: Scalars['Int'];
};


export type MutationRestoreRoleArgs = {
  id: Scalars['Int'];
};

export type Query = {
  __typename?: 'Query';
  roles?: Maybe<Array<Maybe<Roles>>>;
  staff?: Maybe<Array<Maybe<Staff>>>;
  staff2?: Maybe<Array<Maybe<Staff>>>;
  titles?: Maybe<Array<Maybe<Title>>>;
  units?: Maybe<Array<Maybe<Unit>>>;
};


export type QueryStaffArgs = {
  apmId?: InputMaybe<Scalars['String']>;
};


export type QueryStaff2Args = {
  name?: InputMaybe<Scalars['String']>;
  titleId?: InputMaybe<Array<Scalars['ID']>>;
  unitId?: InputMaybe<Scalars['ID']>;
};

export type Roles = {
  __typename?: 'Roles';
  apmId: Scalars['String'];
  deletedBy?: Maybe<Scalars['String']>;
  end?: Maybe<Scalars['DateTime']>;
  id: Scalars['Int'];
  lastUpdated: Scalars['DateTime'];
  role: Scalars['String'];
  staff?: Maybe<Staff>;
  start?: Maybe<Scalars['DateTime']>;
  title: Title;
  unit: Unit;
  unitId: Scalars['String'];
  updatedBy?: Maybe<Scalars['String']>;
};

export type RolesExpired = {
  __typename?: 'RolesExpired';
  apmId: Scalars['String'];
  end?: Maybe<Scalars['DateTime']>;
  expirationDate: Scalars['DateTime'];
  id: Scalars['Int'];
  role: Scalars['String'];
  staff?: Maybe<Staff>;
  start?: Maybe<Scalars['DateTime']>;
  title: Title;
  unit: Unit;
  unitId: Scalars['String'];
  updatedBy?: Maybe<Scalars['String']>;
};

export type Staff = {
  __typename?: 'Staff';
  adminId: Scalars['String'];
  afm?: Maybe<Scalars['String']>;
  amka?: Maybe<Scalars['String']>;
  apmId: Scalars['String'];
  apmIdNew?: Maybe<Scalars['String']>;
  birthdate?: Maybe<Scalars['DateTime']>;
  comment?: Maybe<Scalars['String']>;
  end?: Maybe<Scalars['DateTime']>;
  first: Scalars['String'];
  firstEn?: Maybe<Scalars['String']>;
  gender: staff_gender;
  homeAddress?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  last: Scalars['String'];
  lastEn?: Maybe<Scalars['String']>;
  lastModifiedAtSource?: Maybe<Scalars['DateTime']>;
  lastSync?: Maybe<Scalars['DateTime']>;
  lastUpdated: Scalars['DateTime'];
  middle?: Maybe<Scalars['String']>;
  middleEn?: Maybe<Scalars['String']>;
  nationalityId?: Maybe<Scalars['String']>;
  roles?: Maybe<Array<Roles>>;
  roles_expired?: Maybe<Array<RolesExpired>>;
  start?: Maybe<Scalars['DateTime']>;
  status?: Maybe<staff_status>;
  subUnitId?: Maybe<Scalars['String']>;
  telFax?: Maybe<Scalars['String']>;
  telHome?: Maybe<Scalars['String']>;
  telMobile?: Maybe<Scalars['String']>;
  telOffice?: Maybe<Scalars['String']>;
  titleId: Scalars['String'];
  unit?: Maybe<Unit>;
  unitDetails?: Maybe<Scalars['String']>;
  unitId: Scalars['String'];
};

export type Title = {
  __typename?: 'Title';
  affiliation?: Maybe<title_affiliation>;
  assignedMoreThanOne?: Maybe<Scalars['Boolean']>;
  id: Scalars['ID'];
  lastUpdated: Scalars['DateTime'];
  manualRole?: Maybe<Scalars['Boolean']>;
  nameEn: Scalars['String'];
  nameFemale: Scalars['String'];
  nameMale: Scalars['String'];
  publicRole?: Maybe<Scalars['Boolean']>;
  pyka_tablename?: Maybe<Scalars['String']>;
  relUnitType?: Maybe<title_relUnitType>;
  renewalYears?: Maybe<Scalars['Int']>;
  roleOrder?: Maybe<Scalars['Int']>;
  roles?: Maybe<Array<Roles>>;
  shortName?: Maybe<Scalars['String']>;
  titleCategory?: Maybe<title_titleCategory>;
  type?: Maybe<title_type>;
  unitEditor?: Maybe<Scalars['Boolean']>;
};

export type Unit = {
  __typename?: 'Unit';
  adminUnitId: Scalars['String'];
  adminUnitIdFormatted: Scalars['String'];
  adminUnitIdNew?: Maybe<Scalars['String']>;
  authURL?: Maybe<Scalars['String']>;
  authURLEn?: Maybe<Scalars['String']>;
  dnsAdmin?: Maybe<Scalars['String']>;
  dns_admin_email?: Maybe<Scalars['String']>;
  domain: Scalars['String'];
  id: Scalars['ID'];
  isAcademic: unit_isAcademic;
  isStem?: Maybe<unit_isStem>;
  ldapOU?: Maybe<Scalars['String']>;
  mail?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  nameEn?: Maybe<Scalars['String']>;
  noAccountCreate?: Maybe<unit_noAccountCreate>;
  orderByAthena?: Maybe<Scalars['Int']>;
  parentDomain?: Maybe<Scalars['String']>;
  parentUnitId?: Maybe<Scalars['Int']>;
  primDns?: Maybe<unit_primDns>;
  roles?: Maybe<Array<Roles>>;
  secretaryPersonId?: Maybe<Scalars['Int']>;
  secretaryTels?: Maybe<Scalars['String']>;
  staff?: Maybe<Array<Staff>>;
  syncAdmin?: Maybe<unit_syncAdmin>;
  tStamp: Scalars['DateTime'];
  type?: Maybe<unit_type>;
  unitStatus: unit_unitStatus;
  url?: Maybe<Scalars['String']>;
};

export { staff_gender };

export { staff_status };

export { title_affiliation };

export { title_relUnitType };

export { title_titleCategory };

export { title_type };

export { unit_isAcademic };

export { unit_isStem };

export { unit_noAccountCreate };

export { unit_primDns };

export { unit_syncAdmin };

export { unit_type };

export { unit_unitStatus };

export type WithIndex<TObject> = TObject & Record<string, any>;
export type ResolversObject<TObject> = WithIndex<TObject>;

export type ResolverTypeWrapper<T> = Promise<T> | T;


export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> = ResolverFn<TResult, TParent, TContext, TArgs> | ResolverWithResolve<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterable<TResult> | Promise<AsyncIterable<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = ResolversObject<{
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
  DateTime: ResolverTypeWrapper<Scalars['DateTime']>;
  ID: ResolverTypeWrapper<Scalars['ID']>;
  Int: ResolverTypeWrapper<Scalars['Int']>;
  Mutation: ResolverTypeWrapper<{}>;
  Query: ResolverTypeWrapper<{}>;
  Roles: ResolverTypeWrapper<roles>;
  RolesExpired: ResolverTypeWrapper<roles_expired>;
  Staff: ResolverTypeWrapper<staff>;
  String: ResolverTypeWrapper<Scalars['String']>;
  Title: ResolverTypeWrapper<title>;
  Unit: ResolverTypeWrapper<unit>;
  staff_gender: staff_gender;
  staff_status: staff_status;
  title_affiliation: title_affiliation;
  title_relUnitType: title_relUnitType;
  title_titleCategory: title_titleCategory;
  title_type: title_type;
  unit_isAcademic: unit_isAcademic;
  unit_isStem: unit_isStem;
  unit_noAccountCreate: unit_noAccountCreate;
  unit_primDns: unit_primDns;
  unit_syncAdmin: unit_syncAdmin;
  unit_type: unit_type;
  unit_unitStatus: unit_unitStatus;
}>;

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = ResolversObject<{
  Boolean: Scalars['Boolean'];
  DateTime: Scalars['DateTime'];
  ID: Scalars['ID'];
  Int: Scalars['Int'];
  Mutation: {};
  Query: {};
  Roles: roles;
  RolesExpired: roles_expired;
  Staff: staff;
  String: Scalars['String'];
  Title: title;
  Unit: unit;
}>;

export interface DateTimeScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['DateTime'], any> {
  name: 'DateTime';
}

export type MutationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = ResolversObject<{
  addRole?: Resolver<Maybe<ResolversTypes['Roles']>, ParentType, ContextType, RequireFields<MutationAddRoleArgs, 'apmId' | 'role' | 'unitId'>>;
  createTitle?: Resolver<Maybe<ResolversTypes['DateTime']>, ParentType, ContextType, RequireFields<MutationCreateTitleArgs, 'lastUpdated'>>;
  expireRole?: Resolver<Maybe<ResolversTypes['RolesExpired']>, ParentType, ContextType, RequireFields<MutationExpireRoleArgs, 'id'>>;
  restoreRole?: Resolver<Maybe<ResolversTypes['Roles']>, ParentType, ContextType, RequireFields<MutationRestoreRoleArgs, 'id'>>;
}>;

export type QueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = ResolversObject<{
  roles?: Resolver<Maybe<Array<Maybe<ResolversTypes['Roles']>>>, ParentType, ContextType>;
  staff?: Resolver<Maybe<Array<Maybe<ResolversTypes['Staff']>>>, ParentType, ContextType, RequireFields<QueryStaffArgs, never>>;
  staff2?: Resolver<Maybe<Array<Maybe<ResolversTypes['Staff']>>>, ParentType, ContextType, RequireFields<QueryStaff2Args, never>>;
  titles?: Resolver<Maybe<Array<Maybe<ResolversTypes['Title']>>>, ParentType, ContextType>;
  units?: Resolver<Maybe<Array<Maybe<ResolversTypes['Unit']>>>, ParentType, ContextType>;
}>;

export type RolesResolvers<ContextType = any, ParentType extends ResolversParentTypes['Roles'] = ResolversParentTypes['Roles']> = ResolversObject<{
  apmId?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  deletedBy?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  end?: Resolver<Maybe<ResolversTypes['DateTime']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  lastUpdated?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  role?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  staff?: Resolver<Maybe<ResolversTypes['Staff']>, ParentType, ContextType>;
  start?: Resolver<Maybe<ResolversTypes['DateTime']>, ParentType, ContextType>;
  title?: Resolver<ResolversTypes['Title'], ParentType, ContextType>;
  unit?: Resolver<ResolversTypes['Unit'], ParentType, ContextType>;
  unitId?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  updatedBy?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type RolesExpiredResolvers<ContextType = any, ParentType extends ResolversParentTypes['RolesExpired'] = ResolversParentTypes['RolesExpired']> = ResolversObject<{
  apmId?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  end?: Resolver<Maybe<ResolversTypes['DateTime']>, ParentType, ContextType>;
  expirationDate?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  role?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  staff?: Resolver<Maybe<ResolversTypes['Staff']>, ParentType, ContextType>;
  start?: Resolver<Maybe<ResolversTypes['DateTime']>, ParentType, ContextType>;
  title?: Resolver<ResolversTypes['Title'], ParentType, ContextType>;
  unit?: Resolver<ResolversTypes['Unit'], ParentType, ContextType>;
  unitId?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  updatedBy?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type StaffResolvers<ContextType = any, ParentType extends ResolversParentTypes['Staff'] = ResolversParentTypes['Staff']> = ResolversObject<{
  adminId?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  afm?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  amka?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  apmId?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  apmIdNew?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  birthdate?: Resolver<Maybe<ResolversTypes['DateTime']>, ParentType, ContextType>;
  comment?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  end?: Resolver<Maybe<ResolversTypes['DateTime']>, ParentType, ContextType>;
  first?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  firstEn?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  gender?: Resolver<ResolversTypes['staff_gender'], ParentType, ContextType>;
  homeAddress?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  last?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  lastEn?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  lastModifiedAtSource?: Resolver<Maybe<ResolversTypes['DateTime']>, ParentType, ContextType>;
  lastSync?: Resolver<Maybe<ResolversTypes['DateTime']>, ParentType, ContextType>;
  lastUpdated?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  middle?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  middleEn?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  nationalityId?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  roles?: Resolver<Maybe<Array<ResolversTypes['Roles']>>, ParentType, ContextType>;
  roles_expired?: Resolver<Maybe<Array<ResolversTypes['RolesExpired']>>, ParentType, ContextType>;
  start?: Resolver<Maybe<ResolversTypes['DateTime']>, ParentType, ContextType>;
  status?: Resolver<Maybe<ResolversTypes['staff_status']>, ParentType, ContextType>;
  subUnitId?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  telFax?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  telHome?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  telMobile?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  telOffice?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  titleId?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  unit?: Resolver<Maybe<ResolversTypes['Unit']>, ParentType, ContextType>;
  unitDetails?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  unitId?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type TitleResolvers<ContextType = any, ParentType extends ResolversParentTypes['Title'] = ResolversParentTypes['Title']> = ResolversObject<{
  affiliation?: Resolver<Maybe<ResolversTypes['title_affiliation']>, ParentType, ContextType>;
  assignedMoreThanOne?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  lastUpdated?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  manualRole?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>;
  nameEn?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  nameFemale?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  nameMale?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  publicRole?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>;
  pyka_tablename?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  relUnitType?: Resolver<Maybe<ResolversTypes['title_relUnitType']>, ParentType, ContextType>;
  renewalYears?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  roleOrder?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  roles?: Resolver<Maybe<Array<ResolversTypes['Roles']>>, ParentType, ContextType>;
  shortName?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  titleCategory?: Resolver<Maybe<ResolversTypes['title_titleCategory']>, ParentType, ContextType>;
  type?: Resolver<Maybe<ResolversTypes['title_type']>, ParentType, ContextType>;
  unitEditor?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type UnitResolvers<ContextType = any, ParentType extends ResolversParentTypes['Unit'] = ResolversParentTypes['Unit']> = ResolversObject<{
  adminUnitId?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  adminUnitIdFormatted?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  adminUnitIdNew?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  authURL?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  authURLEn?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  dnsAdmin?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  dns_admin_email?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  domain?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  isAcademic?: Resolver<ResolversTypes['unit_isAcademic'], ParentType, ContextType>;
  isStem?: Resolver<Maybe<ResolversTypes['unit_isStem']>, ParentType, ContextType>;
  ldapOU?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  mail?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  nameEn?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  noAccountCreate?: Resolver<Maybe<ResolversTypes['unit_noAccountCreate']>, ParentType, ContextType>;
  orderByAthena?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  parentDomain?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  parentUnitId?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  primDns?: Resolver<Maybe<ResolversTypes['unit_primDns']>, ParentType, ContextType>;
  roles?: Resolver<Maybe<Array<ResolversTypes['Roles']>>, ParentType, ContextType>;
  secretaryPersonId?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  secretaryTels?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  staff?: Resolver<Maybe<Array<ResolversTypes['Staff']>>, ParentType, ContextType>;
  syncAdmin?: Resolver<Maybe<ResolversTypes['unit_syncAdmin']>, ParentType, ContextType>;
  tStamp?: Resolver<ResolversTypes['DateTime'], ParentType, ContextType>;
  type?: Resolver<Maybe<ResolversTypes['unit_type']>, ParentType, ContextType>;
  unitStatus?: Resolver<ResolversTypes['unit_unitStatus'], ParentType, ContextType>;
  url?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type Staff_GenderResolvers = EnumResolverSignature<{ F?: any, M?: any }, ResolversTypes['staff_gender']>;

export type Staff_StatusResolvers = EnumResolverSignature<{ active?: any, deceased?: any, departed?: any, retired?: any, suspended?: any, transferOut?: any, undetermined?: any, unknown?: any }, ResolversTypes['staff_status']>;

export type Title_AffiliationResolvers = EnumResolverSignature<{ affiliate?: any, employee?: any, faculty?: any, staff?: any, student?: any }, ResolversTypes['title_affiliation']>;

export type Title_RelUnitTypeResolvers = EnumResolverSignature<{ adminDept?: any, adminDir?: any, adminDirGen?: any, adminOffice?: any, committee?: any, department?: any, faculty?: any, facultyDeansOffice?: any, lab?: any, rectorship?: any, school?: any, senate?: any, unit?: any }, ResolversTypes['title_relUnitType']>;

export type Title_TitleCategoryResolvers = EnumResolverSignature<{ affiliate?: any, permAdminStaff?: any, permAssistStaff?: any, permFaculty?: any, student?: any, tempFaculty?: any, tempStaff?: any }, ResolversTypes['title_titleCategory']>;

export type Title_TypeResolvers = EnumResolverSignature<{ INACTIVEtitle?: any, role?: any, staff?: any, student?: any, tempStaff?: any }, ResolversTypes['title_type']>;

export type Unit_IsAcademicResolvers = EnumResolverSignature<{ off?: any, on?: any }, ResolversTypes['unit_isAcademic']>;

export type Unit_IsStemResolvers = EnumResolverSignature<{ off?: any, on?: any }, ResolversTypes['unit_isStem']>;

export type Unit_NoAccountCreateResolvers = EnumResolverSignature<{ off?: any, on?: any }, ResolversTypes['unit_noAccountCreate']>;

export type Unit_PrimDnsResolvers = EnumResolverSignature<{ off?: any, on?: any }, ResolversTypes['unit_primDns']>;

export type Unit_SyncAdminResolvers = EnumResolverSignature<{ active?: any, inactive?: any }, ResolversTypes['unit_syncAdmin']>;

export type Unit_TypeResolvers = EnumResolverSignature<{ adminDept?: any, adminDir?: any, adminDirGen?: any, adminOffice?: any, committee?: any, department?: any, diner?: any, doorGuard?: any, faculty?: any, facultyDeansOffice?: any, gradProgram?: any, indepEntity?: any, lab?: any, library?: any, rectorship?: any, school?: any, senate?: any, thirdParty?: any, unit?: any }, ResolversTypes['unit_type']>;

export type Unit_UnitStatusResolvers = EnumResolverSignature<{ active?: any, inactive?: any }, ResolversTypes['unit_unitStatus']>;

export type Resolvers<ContextType = any> = ResolversObject<{
  DateTime?: GraphQLScalarType;
  Mutation?: MutationResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
  Roles?: RolesResolvers<ContextType>;
  RolesExpired?: RolesExpiredResolvers<ContextType>;
  Staff?: StaffResolvers<ContextType>;
  Title?: TitleResolvers<ContextType>;
  Unit?: UnitResolvers<ContextType>;
  staff_gender?: Staff_GenderResolvers;
  staff_status?: Staff_StatusResolvers;
  title_affiliation?: Title_AffiliationResolvers;
  title_relUnitType?: Title_RelUnitTypeResolvers;
  title_titleCategory?: Title_TitleCategoryResolvers;
  title_type?: Title_TypeResolvers;
  unit_isAcademic?: Unit_IsAcademicResolvers;
  unit_isStem?: Unit_IsStemResolvers;
  unit_noAccountCreate?: Unit_NoAccountCreateResolvers;
  unit_primDns?: Unit_PrimDnsResolvers;
  unit_syncAdmin?: Unit_SyncAdminResolvers;
  unit_type?: Unit_TypeResolvers;
  unit_unitStatus?: Unit_UnitStatusResolvers;
}>;

