The pound `#` symbol indicates that a command should run as a root user, the dollar `$` symbol indicates the normal user. You should replace the symbols `<name>` placeholders with names of your preference.

After `mysql` installation in a LINUX distribution, enable the daemon using `systemctl`.

```
# systemctl start mysql
```

Create a database
```
# mysql -u root -e 'CREATE DATABASE <dbname>;'
```

Create a user and grant all privileges to the database. Use a SECURE password.
```
# mysql -u root -e 'GRANT ALL PRIVILEGES ON <dbname>.* TO "<username>"@"localhost" IDENTIFIED BY "<password>";'
```

Execute the db creating `.sql` scripts with your user
```
$ mysql -u <username> --password=<password> <dbname> < <scriptname>
```

# CHANGES
Add Default empty string `''` value to `adminUnitIdFormatted` of table `unit`.
